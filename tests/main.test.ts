import { generateAccount } from "algosdk";
import { expect } from "chai";
import { verifyAddress } from "../src/index";

describe("unit tests", (): void => {
  it("gets verification from a verified address", async (): Promise<void> => {
    const result = await verifyAddress(
      "DAVIDXFL3HHNFMAVQH7M7Q3NV3OYV35MP24QMPHODCA3RTPPWVH5J5ZHVI",
      "mainnet"
    );
    expect(result.ipfsHash).is.equal(
      "bafkreif5rvwqy3e5e7mnb5z7wpwziki4kofwsqqpisj4smbakp7xrc7w5m"
    );
    expect(result.isVerified).is.equal(true);
  });
  it("gets verification from a unverified address", async (): Promise<void> => {
    const result = await verifyAddress(generateAccount().addr, "testnet", {
      url: "https://algoindexer.testnet.algoexplorerapi.io",
    });
    expect(result.isVerified).is.equal(false);
  });
});
