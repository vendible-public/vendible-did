"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.verifyAddress = void 0;
const algosdk_1 = require("algosdk");
const failed = {
    isVerified: false,
    ipfsHash: undefined,
};
function isValidHttpUrl(url) {
    try {
        const urlParse = new URL(url);
        return urlParse.protocol === "http:" || urlParse.protocol === "https:";
    }
    catch (_) {
        return false;
    }
}
function verifyAddress(address, network = "mainnet", indexerParams) {
    var _a;
    return __awaiter(this, void 0, void 0, function* () {
        if (indexerParams) {
            if (!isValidHttpUrl(indexerParams === null || indexerParams === void 0 ? void 0 : indexerParams.url)) {
                throw new Error("Invalid Url Address");
            }
        }
        const indexer = new algosdk_1.Indexer({ "X-API-KEY": (indexerParams === null || indexerParams === void 0 ? void 0 : indexerParams.token) || "" }, (indexerParams === null || indexerParams === void 0 ? void 0 : indexerParams.url) || "https://algoindexer.algoexplorerapi.io/", (indexerParams === null || indexerParams === void 0 ? void 0 : indexerParams.port) || "");
        if (!(0, algosdk_1.isValidAddress)(address)) {
            throw new Error("Invalid Algo Address");
        }
        try {
            const data = (yield indexer
                .lookupAccountByID(address)
                .do());
            if (!data.account || !data.account["apps-local-state"])
                return failed;
            for (const value of ((_a = data.account["apps-local-state"].find((item) => item.id === (network === "mainnet" ? 651045146 : 75893547))) === null || _a === void 0 ? void 0 : _a["key-value"]) || []) {
                if (Buffer.from(value.key, "base64").toString() === "ipfs_hash") {
                    return {
                        isVerified: true,
                        ipfsHash: Buffer.from(value.value.bytes, "base64").toString(),
                    };
                }
            }
        }
        catch (e) {
            if (e instanceof Error) {
                if (e.message.includes("no accounts found for"))
                    return failed;
                throw new Error("Error to get data from Indexer API");
            }
        }
        return failed;
    });
}
exports.verifyAddress = verifyAddress;
