interface AccountResponse {
    isVerified: boolean;
    ipfsHash?: string;
}
export declare function verifyAddress(address: string, network?: string, indexerParams?: {
    url: string;
    token?: string;
    port?: number;
}): Promise<AccountResponse>;
export {};
