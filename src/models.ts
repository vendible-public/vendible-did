export interface IndexerApiData {
  account: Account;
  "current-round": number;
}

export interface Account {
  address: string;
  amount: number;
  "amount-without-pending-rewards": number;
  "apps-local-state": AppsLocalState[];
  "apps-total-schema": Schema;
  assets: Asset[];
  "created-at-round": number;
  deleted: boolean;
  "pending-rewards": number;
  "reward-base": number;
  rewards: number;
  round: number;
  "sig-type": string;
  status: string;
}

export interface AppsLocalState {
  deleted: boolean;
  id: number;
  "key-value": KeyValue[];
  "opted-in-at-round": number;
  schema: Schema;
}

export interface KeyValue {
  key: string;
  value: Value;
}

export interface Value {
  bytes: string;
  type: number;
  uint: number;
}

export interface Schema {
  "num-byte-slice": number;
  "num-uint": number;
}

export interface Asset {
  amount: number;
  "asset-id": number;
  creator: string;
  deleted: boolean;
  "is-frozen": boolean;
  "opted-in-at-round": number;
}
