import { isValidAddress, Indexer } from "algosdk";
import { IndexerApiData } from "./models";

interface AccountResponse {
  isVerified: boolean;
  ipfsHash?: string;
}

const failed: AccountResponse = {
  isVerified: false,
  ipfsHash: undefined,
};

function isValidHttpUrl(url: string): boolean {
  try {
    const urlParse = new URL(url);
    return urlParse.protocol === "http:" || urlParse.protocol === "https:";
  } catch (_) {
    return false;
  }
}

export async function verifyAddress(
  address: string,
  network: string = "mainnet",
  indexerParams?: { url: string; token?: string; port?: number }
): Promise<AccountResponse> {
  if (indexerParams) {
    if (!isValidHttpUrl(indexerParams?.url)) {
      throw new Error("Invalid Url Address");
    }
  }
  const indexer = new Indexer(
    { "X-API-KEY": indexerParams?.token || "" },
    indexerParams?.url || "https://algoindexer.algoexplorerapi.io/",
    indexerParams?.port || ""
  );
  if (!isValidAddress(address)) {
    throw new Error("Invalid Algo Address");
  }

  try {
    const data: IndexerApiData = (await indexer
      .lookupAccountByID(address)
      .do()) as IndexerApiData;
    if (!data.account || !data.account["apps-local-state"]) return failed;

    for (const value of data.account["apps-local-state"].find(
      (item) => item.id === (network === "mainnet" ? 651045146 : 75893547)
    )?.["key-value"] || []) {
      if (Buffer.from(value.key, "base64").toString() === "ipfs_hash") {
        return {
          isVerified: true,
          ipfsHash: Buffer.from(value.value.bytes, "base64").toString(),
        };
      }
    }
  } catch (e: unknown) {
    if (e instanceof Error) {
      if (e.message.includes("no accounts found for")) return failed;
      throw new Error("Error to get data from Indexer API");
    }
  }
  return failed;
}
