# Vendible ID protocol usage

The Vendible identification protocol allows developers to determine if an Algorand L1 public address is paired with an unique and valid identity.

## Installation

---

`npm install @vendible/vendible-did`

or

`yarn add @vendible/vendible-did`

## Import

---

```typescript
import { verifyAddress } from "@vendible/vendible-did";

//OR

const { verifyAddress } = require("@vendible/vendible-did");
```

If you have any problem in the build, you can also use the compiled version

```typescript
import { verifyAddress } from "@vendible/vendible-did/dist";

//OR

const { verifyAddress } = require("@vendible/vendible-did/dist");
```

## Usage

---

The `verifyAddress` function takes two parameters, the Algorand address to verify and optionally, an object to build the Algorand Indexer that will be used to make the query. This allows the use of alternate Indexers other than [AlgoExplorer's mainnet indexer](https://algoindexer.algoexplorerapi.io/), which is set by default.

You can also select if you want to make the request in the mainet, or testnet. By default, results are obtained from the mainet.

This function returns an object containing a boolean value that indicates if the user is verified or not and the possible IPFS hash associated with the address.

```typescript
//using default indexer
const result = await verifyAddress("insert address here");

//response if verified
// {
//   isVerified: true,
//   ipfsHash: 'hash value'
// }

//using custom indexer
const resultUnverified = await verifyAddress(
  "insert address here",
  "testnet || mainnet", //Network
  {
    url: "insert indexer URL here",
    token: "insert token here",
    port: "insert port here",
  }
);

//response if unverified
// {
//   isVerified: false,
//   ipfsHash: undefined
// }
```

## Behind the Scenes

---

Any address paired with an unique and valid identity will be registered in the Algorand dApp ID [651045146](https://algoexplorer.io/application/651045146) for Mainnet and [75893547](https://testnet.algoexplorer.io/application/75893547) for Testnet. This package simplifies the code required to query the Algorand blockchain for a valid DID.
